if (typeof (MenuOderApp) === 'undefined') MenuOderApp = {};

MenuOderApp.MenuOrderChecker = (function ($) {
  var grandTotal=0;
 
  var config = {
    seafoodError: "Sorry, you can't choose Prawn Cocktail and the Salmon Fillet",
    noMainError: 'You need to choose a main',
	onlyMainError: 'You need to choose a main AND another option from desserts or starters',
	noSelectionError: 'Please select at least two courses with at least one as a main'
  };
  
  //****//
  // would use AJAX get call to retrieve data from server
  //Would convert other main and desserts to JSON objs but no time
  //******//
  
  //var objectFromJson = $.parseJSON('{"starter": [{"course":"starter", "mealValue": "soupDay", "mealName": "Soup of the day", "price":3, "quantity":2}, {"course":"starter", "mealValue": "Pate", "mealName": "Pâté", "price":5, "quantity":9},{"course":"starter", "mealValue": "bruschetta", "mealName": "Bruschetta", "price":4.5, "quantity":2},{"course":"starter", "mealValue": "prawnCocktail", "mealName": "Prawn Cocktail","price":6, "quantity":1}]}');
  var objectFromJson = $.parseJSON('{"starter": [{"course":"starter", "mealValue": "soupDay", "mealName": "Soup of the day", "price":3, "quantity":2}, {"course":"starter", "mealValue": "Pate", "mealName": "Pâté", "price":5, "quantity":9},{"course":"starter", "mealValue": "bruschetta", "mealName": "Bruschetta", "price":4.50, "quantity":2},{"course":"starter", "mealValue": "prawnCocktail", "mealName": "Prawn Cocktail","price":6, "quantity":1}],"desserts": [{"course":"desserts", "mealValue": "stickyToffeePudding", "mealName": "Sticky toffee pudding", "price":4, "quantity":20}, {"course":"desserts", "mealValue": "tiramisu", "mealName": "Tiramisu", "price":4.50, "quantity":9},{"course":"desserts", "mealValue": "cheeseCake", "mealName": "Cheesecake", "price":4, "quantity":2},{"course":"desserts", "mealValue": "iceCream", "mealName": "Ice cream","price":3.50, "quantity":10}]}');
  
  var $radioButtons = "#formBody input";

  var setup = function () {
	makeList(objectFromJson);
	correctForStock($radioButtons);
	
	$('#formBody').on('change ', 'input' ,eventsMethods);
	$('#orderButton').on('click', chkSubmission);
  };

  // *-* utility methods *-*
  
  var seafoodCombo = function (itemSelected) {
	  console.log('seafoodCombo func');
	  var starterItem= $('input[name=starter]:checked', '#formBody').val();
	  var mainItem= $('input[name=main]:checked').val();
	  
	  if(starterItem === 'prawnCocktail' && mainItem === 'salmonFillet' ){
		itemSelected.prop('checked',false);
		console.log('prawnCocktail or salmonFillet has been selected');
		displayError(config.seafoodError); // display error
	  }
  };
 
  var calculateTotal = function() { 
	var total= 0;
	$("#formBody input:checked").each(function() {
		var value = $(this).attr('data-price');
		total += parseFloat(value);
	});
    return total;
  };
  var setTotal = function(total) { 
	$("#totalValue").html(total);
  };
  
  var displayError = function(errMsg){
	$('#errorBox').toggle().html(errMsg);
  };
  var chkSubmission = function(){
	  console.log('chkSubmission');
	  if( $('input:radio:checked').length === ($('input:radio:checked').length/2) ){
		  displayError(config.noSelectionError);
	  }
	  else if ($("input[name=main]:checked").length === 0){
		 console.log('noMainError');
		displayError(config.noMainError); // display error
	  }
	  else if (($("input[name=starter]:checked").length === 0 && $("input[name=desserts]:checked").length === 0) && $("input[name=main]:checked")  ){
		 console.log('only Main selected Error');
		displayError(config.onlyMainError); // display error
	  }
	  else{
		 $('#containerForm').hide();	
		 $('#confirmationBox').show();
	  }
  }
  
  var clearError = function(errMsg){
		 $('#errorBox').hide();
  };

  // *-* event methods *-*
  var eventsMethods = function () {
	clearError();
	var itemSelected= $(this);
    seafoodCombo(itemSelected);
	total  = calculateTotal();
	setTotal(total);
  };
  var makeList = function(objectFromJson){
	  console.log('makeList called');
	  var eleContainerStart = '<div class="menuDescriptionWrapper"><div class="menuDescription">';
	  
	  $.each(objectFromJson.starter, function(k, v){
        var _menuItem = '<label class="block"><input type="radio" value="' + v.mealValue + '" name="' + v.course + '" class="' + v.course + '" data-price="' + parseFloat(v.price) + '" data-qty="'+ v.quantity+ '">' + v.mealName+ '</label>';
		var _starterOption =  eleContainerStart + _menuItem + '</div><div class="price"> £ ' + parseFloat(v.price) +'</div></div>';
        $('#startersSection').append(_starterOption);
     });
	 $.each(objectFromJson.desserts, function(k, v){
        var _menuItem = '<label class="block"><input type="radio" value="' + v.mealValue + '" name="' + v.course + '" class="' + v.course + '" data-price="' + parseFloat(v.price) + '" data-qty="'+ v.quantity+ '">' + v.mealName+ '</label>';
		var _dessertOption =  eleContainerStart + _menuItem + '</div><div class="price"> £ ' + parseFloat(v.price) +'</div></div>';
        $('#dessertSection').append(_dessertOption);
     });
	 
  };
  
  var correctForStock = function (radioButtons) {
	  console.log('checkStock func');
	  $(radioButtons).each(function() {
		var value = parseFloat($(this).attr('data-qty'));
		if(value === 0){
			$(this).prop("disabled",true);
			console.log($(this).parent("div"));
			$(this).parent('label').html($(this).parent('label').clone().html() +' - No more available -').css( "color", "red" );
		}
	  });
  };
  

  // expose public methods
  return {
    setup: setup,
	//correctForStock:correctForStock, //test purposes UNCOMMIT TO GET TESTS WORKING
	//setTotal: setTotal//test purposes //test purposes UNCOMMIT TO GET TESTS WORKING
  };
})(jQuery);

jQuery(document).ready(MenuOderApp.MenuOrderChecker.setup);