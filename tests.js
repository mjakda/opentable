
// ensure all the required form elements are in place
test( "All required form elements exist", 13, function() {
    ok($("input[value='soupDay']").length != 0, "soupDay radio button selection exists");
	ok($("input[value='Pate']").length != 0, "Pate radio button selection exists");
	ok($("input[value='bruschetta']").length != 0, "bruschetta radio button selection exists");
	ok($("input[value='prawnCocktail']").length != 0, "prawnCocktail radio button selection exists");
	ok($("input[value='steak']").length != 0, "steak radio button selection exists");
    ok($("input[value='Meatballs']").length != 0, "Meatballs radio button selection exists");
    ok($("input[value='salmonFillet']").length != 0, "salmonFillet radio button selection exists");
    ok($("input[value='vegetarianLasagna']").length != 0, "vegetarianLasagna radio button selection exists");
    ok($("input[value='stickyToffeePudding']").length != 0, "stickyToffeePudding radio button selection exists");
    ok($("input[value='tiramisu']").length != 0, "tiramisu radio button selection exists");
	ok($("input[value='cheeseCake']").length != 0, "cheeseCake radio button selection exists");
	ok($("input[value='iceCream']").length != 0, "iceCream radio button selection exists");
	ok($("#orderButton").length != 0, "order button exists");
	
});
module('OpenTableModule', {
	setup: function() {
		// setup code
	},
	teardown: function() {
		$('#containerForm').show();	
		 $('#confirmationBox').hide();
	}
});
test( "test total menu selection calculation output for two valid options", 1, function() {
   
    $("input:radio[value='soupDay']").attr('checked',true);
    $("input:radio[value='steak']").attr('checked',true);
	
    var event, $submitButton = $("#orderButton");
 	var event2, $inputField = $('#formBody input');
    // trigger event
    event = $.Event( "click" );
	event2 = $.Event('change');
	$inputField.trigger(event2);
    $submitButton.trigger(event);
 
    var _testTotal = $("#totalValue").text();
    equal(_testTotal, 21, "menu choice correctly totals" );
});
//test when main is ONLY selected
test( "test when main is ONLY selected", 1, function() {
	
   $('input').prop('checked', false);$('input:radio').removeAttr('checked');
    $("input:radio[value='Meatballs']").attr('checked',true);
	
    var event, $submitButton = $("#orderButton");
 	var event2, $inputField = $('#formBody input');
    // trigger event
    event = $.Event( "click" );
	event2 = $.Event('change');
	$inputField.trigger(event2);
    $submitButton.trigger(event);
 
    var _errorMsg = $("#errorBox").text();
    equal(_errorMsg, "You need to choose a main AND another option from desserts or starters" );
});

//*test total menu selection calculation output for three valid options"
//test( "test total menu selection calculation output for three valid options", 1, function() {
//    $('input:radio').removeAttr('checked');
//	$('input').prop('checked', false);
//    $("input:radio[value='Pate']").attr('checked',true);
//    $("input:radio[value='salmonFillet']").attr('checked',true);
//	 $("input:radio[value='cheeseCake']").attr('checked',true);
//	
//    var event, $submitButton = $("#orderButton");
// 	var event2, $inputField = $('#formBody input');
//    // trigger event
//    event = $.Event( "click" );
//	event2 = $.Event('change');
//	$inputField.trigger(event2);
//    $submitButton.trigger(event);
// 
//    var _testTotal = $("#totalValue").text();
//    equal(_testTotal, 23, "menu choice correctly  for 3 items" );
//});



//// Salmon and Prawn cocktail wrong combo
test( "test error message when Salmon and Prawn Cocktail is chosen", 1, function() {
    $('input').prop('checked', false);$('input:radio').removeAttr('checked');
    $("input:radio[value='prawnCocktail']").attr('checked',true);
    $("input:radio[value='salmonFillet']").attr('checked',true);
	
    var event, $submitButton = $("#orderButton");
 	var event2, $inputField = $('#formBody input');
    // trigger event
    event = $.Event( "click" );
	event2 = $.Event('change');
	$inputField.trigger(event2);
    $submitButton.trigger(event);
 
    var _errorMsg = $("#errorBox").text();
    equal(_errorMsg, "Sorry, you can't choose Prawn Cocktail and the Salmon Fillet", "correct error msg" );
});

//// Test for when no course at all has been selected and the "make order" button clicked
test( "test error message when no selection has been made and make order button clicked", 1, function() {
   $('input').prop('checked', false);
    var event, $submitButton = $("#orderButton");
 	
    // trigger event
    event = $.Event( "click" );
	
    $submitButton.trigger(event);
 
    var _errorMsg = $("#errorBox").text();
    equal(_errorMsg, "Please select at least two courses with at least one as a main", "correct error msg" );
});

//// test error message when main course has NOT been chosen
test( "test error message when main course has NOT been chosen", 1, function() {
   $('input').prop('checked', false);$('input:radio').removeAttr('checked');
    $("input:radio[value='prawnCocktail']").attr('checked',true);
    $("input:radio[value='iceCream']").attr('checked',true);
	
    var event, $submitButton = $("#orderButton");
 	var event2, $inputField = $('#formBody input');
    // trigger event
    event = $.Event( "click" );
	event2 = $.Event('change');
	$inputField.trigger(event2);
    $submitButton.trigger(event);
 
    var _errorMsg = $("#errorBox").text();
    equal(_errorMsg, "You need to choose a main", "correct error msg" );
});

// test if cheeseCake radio button is disabled if quantity is zero
test( "test if cheeseCake radio button is disabled if quantity is zero", 1, function() {
    $('input').prop('checked', false);
    $("input:radio[value='cheeseCake']").attr('data-qty','0');
	var $radioButtons = "#formBody input";
	MenuOderApp.MenuOrderChecker.correctForStock($radioButtons);
	//$("input:radio[value='cheeseCake']").prop("disabled",true);
   
	equal($("input:radio[value='cheeseCake']").prop('disabled'), true, "cheeseCake radio button is disabled" );
});
// Set Total function test
test( "test if Test Total function work", 1, function() {
	var _dummyTotal = 42
   	MenuOderApp.MenuOrderChecker.setTotal(_dummyTotal)
	equal($("#totalValue").html(), 42, "set total function works" );
});



